package javacollections;

public class Drumkit {

    private String DrumPart;
    private String Size;
    private String Color;
    private String Brand;

    public Drumkit(String DrumPart, String Size, String Color, String Brand) {
        this.DrumPart = DrumPart;
        this.Size = Size;
        this.Color = Color;
        this.Brand = Brand;
    }

    public String toString() {
        return "DrumPart: " + DrumPart + "Size: " + Size + "Color: " + Color + "Brand: " + Brand;
    }
}
