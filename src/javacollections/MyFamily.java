package javacollections;

public class MyFamily {

    private String MyName;

    public MyFamily(String MyName) {
        this.MyName = MyName;
    }

    public String toString() {
        return "MyName: " + MyName;
    }
}
