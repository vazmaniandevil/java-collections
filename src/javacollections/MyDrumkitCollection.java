package javacollections;

import java.util.*;

public class MyDrumkitCollection {

    public static void main(String[] args) {

        //Print
        System.out.println("\n\n***LIST***\n ");
        System.out.println("My Drum Kit\n ");

        List<Drumkit> listOne = new LinkedList<Drumkit>();
        listOne.add(new Drumkit("Tom 1 ", "8 Inches ", "Black ", "Pearl "));
        listOne.add(new Drumkit("Tom 2 ", "10 Inches ", "Blue ", "Pearl "));
        listOne.add(new Drumkit("Tom 3 ", "12 Inches ", "Purple ", "Pearl "));
        listOne.add(new Drumkit("Kick ", "22 Inches ", "Red ", "DW "));
        listOne.add(new Drumkit("Snare", "14 Inches", "Yellow", "DW"));
        listOne.add(new Drumkit("Double Pedals ", "None ", "Metallic ", "DW "));

        for (Drumkit drumkit : listOne) {
            System.out.println(drumkit);
        }

        //Print
        /*System.out.println("\n\n***TREESET***\n ");
        System.out.println("My Drum Kit\n ");

        TreeSet setOne = new TreeSet();
        setOne.add(new Drumkit("Tom 4 ", "8 Inches ", "Black ", "Pearl "));
        setOne.add(new Drumkit("Tom 5 ", "10 Inches ", "Blue ", "Pearl "));
        setOne.add(new Drumkit("Tom 6 ", "12 Inches ", "Purple ", "Pearl "));
        setOne.add(new Drumkit("Stand", "22 Inches ", "Red ", "DW "));
        setOne.add(new Drumkit("Snare", "14 Inches", "Yellow", "DW"));
        setOne.add(new Drumkit("Double Pedals ", "None ", "Metallic ", "DW "));

        for (Object drumkit : setOne) {
            System.out.println(drumkit);
        }*/

        //Print
        System.out.println("\n\n***QUEUE***\n ");
        System.out.println("My Family\n ");

        //Example of Queue
        Queue<MyFamily> queueOne = new PriorityQueue<MyFamily>();
        queueOne.add(new MyFamily("Wesley"));

        //Iterator is used to iterate through the List and then to print it
        //Prints in alphabetical order
        //Prints first letter uppercase first
        Iterator<MyFamily> iterator = queueOne.iterator();
        while (iterator.hasNext()) {
            System.out.println(queueOne.poll());
        }

    }

}
