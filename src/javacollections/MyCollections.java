package javacollections;

import java.util.*;

public class MyCollections {

    public static void main(String[] args) {

        //Print
        System.out.println("\n\n***LIST***\n ");
        System.out.println("My Family\n ");

        //Example of ArrayList
        List listone = new ArrayList();
        listone.add("Wesley");
        listone.add("Tiffany");
        listone.add("John");
        listone.add("Charles");
        listone.add("Jack (coming soon)");

        //For is used to go through the List and then to print it
        for (Object str : listone) {
            System.out.println((String)str);
        }

        //Print
        System.out.println("\n\n***SET***\n ");
        System.out.println("My Family\n ");

        //Example of Set
        Set setone = new TreeSet();
        setone.add("Wesley");
        setone.add("Tiffany");
        setone.add("John");
        setone.add("Charles");
        setone.add("Jack (coming soon)");
        //Duplicates are not printed
        setone.add("Wesley");

        //For is used to go through the List and then to print it
        for (Object str : setone) {
            System.out.println((String)str);
        }

        //Print
        System.out.println("\n\n***QUEUE***\n ");
        System.out.println("My Family\n ");

        //Example of Queue
        Queue queueone = new PriorityQueue();
        queueone.add("Wesley");
        /*queueone.add("Tiffany");
        queueone.add("John");
        queueone.add("charles");
        queueone.add("jack (coming soon)");
        queueone.add("wesley");*/

        //Iterator is used to iterate through the List and then to print it
        //Prints in alphabetical order
        //Prints first letter uppercase first
        Iterator iterator = queueone.iterator();
        while (iterator.hasNext()) {
            System.out.println(queueone.poll());
        }

        System.out.println("\n\n***TREESET***\n ");
        System.out.println("My Family\n ");

        TreeSet treesetone = new TreeSet();
        treesetone.add("Wesley");
        treesetone.add("Tiffany");
        treesetone.add("John");
        treesetone.add("Charles");
        treesetone.add("Jack");
        //Duplicates are not printed
        treesetone.add("Jack");

        //Prints in alphabetical order
        System.out.println(treesetone);

        System.out.println("***Comparator***");
        List<Drumkit> myList = new LinkedList<Drumkit>();
        myList.add(new Drumkit("Tom 1 ", "8 Inches ", "Black ", "Pearl "));
        myList.add(new Drumkit("Tom 2 ", "10 Inches ", "Blue ", "Pearl "));
        myList.add(new Drumkit("Tom 3 ", "12 Inches ", "Purple ", "Pearl "));
        myList.add(new Drumkit("Kick ", "22 Inches ", "Red ", "DW "));
        myList.add(new Drumkit("Snare", "14 Inches", "Yellow", "DW"));
        myList.add(new Drumkit("Double Pedals ", "None ", "Metallic ", "DW "));

        for (Drumkit drumkit : myList) {
            System.out.println(drumkit);
        }

    }
}
